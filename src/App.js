import React from "react";
import "./App.css";
import Header from "./components/Header";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Profile from "./pages/Profile";
import Product from "./pages/Product";
import PrivateRoute from "./helpers/PrivateRoute";

import "bootstrap/dist/css/bootstrap.min.css";
import { Route, Switch, Redirect } from "react-router-dom";
import { authToken } from "./helpers/Auth";

const routes = {
  home: "/home",
  login: "/login",
  register: "/register",
  profile: "/profile",
  product: "/product",
};

function App() {
  return (
    <div className="App">
      <Header authToken={authToken} />
      <div className="container">
        <Switch>
          <Redirect exact from="/" to={routes.login}></Redirect>
          <Route exact path={routes.login} component={Login}></Route>
          <Route exact path={routes.register} component={Register}></Route>
          <PrivateRoute
            exact
            path={routes.home}
            component={Home}
          ></PrivateRoute>
          <PrivateRoute
            exact
            path={routes.profile}
            component={Profile}
          ></PrivateRoute>
          <PrivateRoute
            exact
            path={routes.product}
            component={Product}
          ></PrivateRoute>
        </Switch>
      </div>
    </div>
  );
}

export default App;
