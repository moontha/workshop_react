import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import "./Header.css";

export default function Header(props) {
  const [state, setstate] = useState(true);

  useEffect(() => {
    console.log("666");
    isLogin();

    console.log(state);
  }, [state]);

  const isLogin = async () => {
    console.log("555");
    if (localStorage.getItem("id")) {
      return await setstate(true);
    }
    await setstate(false);
  };

  const guest = (
    <ul className="navbar-nav ml-auto">
      <li className="nav-item">
        <NavLink
          to="/login"
          className="nav-link"
          activeStyle={{ color: "#FFFFF" }}
        >
          Login
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink
          to="/register"
          className="nav-link"
          activeStyle={{ color: "#FFFFF" }}
        >
          Register
        </NavLink>
      </li>
    </ul>
  );

  const user = (
    <ul className="navbar-nav ml-auto">
      <li className="nav-item">
        <NavLink
          to="/product"
          className="nav-link"
          activeStyle={{ color: "#FFFFF" }}
          onClick={(e) => {
            setstate("");
            
          }}
        >
         My Product
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink
          to="/profile"
          className="nav-link"
          activeStyle={{ color: "#FFFFF" }}
          onClick={(e) => {
            setstate("");
            
          }}
        >
          Profile
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink
          to="/home"
          className="nav-link"
          activeStyle={{ color: "#FFFFF" }}
          onClick={(e) => {
            setstate("");
            localStorage.removeItem("id");
          }}
        >
          Logout
        </NavLink>
      </li>
    </ul>
  );

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark ">
        
      <a className="navbar-brand" href="/home">
          ChaCake
        </a>

        <div className="collapse navbar-collapse" id="navbarNav">
          {state ? user : guest}
        </div>
      </nav>
    </div>

  );
}
