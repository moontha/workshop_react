import React from "react";

export default function ProfileComponent(props) {
  const { user } = props;
  return (
    <div>
      {user.map((user, index) => {
        return (

          <div className="container">
  <div className="profile-area d-flex align-items-center justify-content-center">
      <div className="card-body"> 
      <h1 className="card-title">My Profile</h1>
      <div className="row">
      <div class="col">
     <div className="card-img-top">
            <img src={ process.env.PUBLIC_URL + 'images/profile.gif' } width="600"></img>
      </div>
      </div>
      <div class="col">
   
        <div className="card-body">
          
          <p className="card-text"></p>
          <h4>Name: {user.name}</h4>
          <h4>Age: {user.age}</h4>
          <h4>Salary: {user.salary}</h4>
          <div className="btn-edit">
          <a
                href="{{ route('profile.edit', $user->id)}}"
                class="btn btn-sm btn-outline-info"
              >
                Edit
              </a>
          </div>
  </div>

 </div>
</div>
    </div>
  </div>
</div>
        );
      })}
    </div>
  );
}
