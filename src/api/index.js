import axios from "axios";

const url = "http://203.154.59.14:3000/api/v1";

export const login = (user) => axios.post(url + "/login", user);

export const register = (user) => axios.post(url + "/register", user);

export const profile = (id) => axios.get(url + "/users/" + id);

export const product = () => axios.get(url + "/products/");



// export const getAllItem = () => {
//     return new Promise((resolve, reject) => {
//       axios.get(url + '/products').then(res => {
//         resolve(res.data)
//       })
//     })
//   }
  
//   export const getAllByIdItem = (id) => {
//     return new Promise((resolve, reject) => {
//       axios.get(url + '/products/' + id).then(res => {
//         resolve(res.data)
//       })
//     })
//   }
  
//   export const createItem = (item) => {
//     return new Promise((resolve, reject) => {
//       axios.post(url + '/products', item).then(res => {
//         resolve(res.data)
//       })
//     })
//   }
  
//   export const editItem = (id, item) => {
//     return new Promise((resolve, reject) => {
//       axios.put(url + '/products/' + id, item).then(res => {
//         resolve(res.data)
//       })
//     })
//   }
  
//   export const deleteItem = (id) => {
//     return new Promise((resolve, reject) => {
//       axios.delete(url + '/products/' + id).then(res => {
  
//         resolve(res)
//       })
//     })
//   }
  