import React, { useState } from "react";
import { login } from "../api";
import "./Login.css";

export default function Login(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    let user = {
      username,
      password,
    };
    try {
      const result = await login(user);
      console.log(result);
      if (result.data.status === "success") {
        localStorage.setItem("id", result.data.data._id);
        return props.history.push("/home");
      }
    } catch (error) {}
  };

  return (
    <div>
      <div className="login-area d-flex align-items-center justify-content-center">
      <div className="card" style={{ width: "29rem" }}>
          <div className="card-body">
        <div style={{ textAlign: 'center' }}>
            <img src={ process.env.PUBLIC_URL + 'images/login.gif' } width="300"></img>
          </div>
          <h3 className="text-center mb-3">Login</h3>
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              
              <input
                type="text"
                className="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                placeholder="Username"
                onChange={(e) => setUsername(e.target.value)}
              />
              
            </div>
            <div className="form-group">
              
              <input
                type="password"
                className="form-control"
                id="exampleInputPassword1"
                placeholder="Password"
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            <button
              type="submit"
              className="btn btn-warning btn-block login-btn"
            >
              Login
            </button>
          </form>
        </div>
      </div>
    </div>
    </div>
  );
}
