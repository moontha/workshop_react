import React, { useState, useEffect } from "react";
import { product } from "../api";

export default function Home() {
  const [state, setstate] = useState([]);

  useEffect(() => {
    fetchProduct();
  }, []);
  const fetchProduct = async () => {
    try {
      const result = await product();
      setstate(result.data.data);
    } catch (error) {}
  };
  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>Title</th>
            <th>Detail</th>
            <th>Stock</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {state.map((product, index) => {
            return (
              <tr key={index}>
                <td>{product.title}</td>
                <td>{product.detail}</td>
                <td>{product.stock}</td>
                <td>{product.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
