import React, { useState } from "react";
import { register } from "../api";
import "./Register.css";
export default function Register(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [salary, setSalary] = useState("");

  const save = async (e) => {
    e.preventDefault();
    let user = {
      username,
      password,
      name,
      age,
      salary,
    };
    try {
      const result = await register(user);
      console.log(result);
      if (result.data.status === "success") {
        return props.history.push("/home");
      }
    } catch (error) {
      console.log(error);
    }
  };


  return (
    <div >
      <div className="register-area d-flex align-items-center justify-content-center">
        <div className="card" style={{ width: "28rem" }}>
          <div className="card-body">
          <div style={{ textAlign: 'center' }}>
            <img src={ process.env.PUBLIC_URL + 'images/regis.gif' } width="180"></img>
          </div>
          <div className="content-top">
              <h3>Register</h3>
            </div>
            <div className="content-center text-left">
          <form onSubmit={save}>
            <div className="form-group ">
            <label htmlFor="username">Username</label>
             
                {" "}
                <input
                  type="text"
                  className="form-control"
                  id="exampleInputUsername1"
                  aria-describedby="emailHelp"
                  onChange={(e) => setUsername(e.target.value)}
                />
              </div>
           
            <div className="form-group ">
            <label htmlFor="password">Password</label>
             
                {" "}
                <input
                  type="password"
                  className="form-control"
                  id="exampleInputPassword1"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>
            
            <div className="form-group ">
            <label htmlFor="name">Name</label>
             
               
                <input
                  type="text"
                  className="form-control"
                  id="exampleInputName1"
                  onChange={(e) => setName(e.target.value)}
                />
              </div>
           
            <div className="form-group ">
            <label htmlFor="age">Age</label>
             
                
                <input
                  type="text"
                  className="form-control"
                  id="exampleInputAge1"
                  onChange={(e) => setAge(e.target.value)}
                />
              </div>
           
            <div className="form-group ">
            <label htmlFor="salary">Salary</label>
             
                <input
                  type="text"
                  className="form-control"
                  id="exampleInputSalary1"
                  onChange={(e) => setSalary(e.target.value)}
                />
              </div>
           
            <button
              type="submit"
              className="btn btn-warning btn-block register-btn"
            >
              Register
            </button>
          </form>
        </div>
      </div>
    </div>
    </div>
    </div>
  );
}
